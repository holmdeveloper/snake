import React from "react";
import "./ExitConfirmationModal.css";
const ExitConfirmationModal = ({ onCancel, onConfirm }) => {
  return (
    <div className="exit-modal">
      <p>Are you sure you want to exit the game?</p>
      <button onClick={onCancel}>Cancel</button>
      <button onClick={onConfirm}>Exit</button>
    </div>
  );
};

export default ExitConfirmationModal;
