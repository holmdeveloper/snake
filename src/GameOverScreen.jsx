import React from "react";
import "./GameOverScreen.css";

const GameOverScreen = ({ score, onRestart }) => {
  return (
    <div className="game-over-screen">
      <h2>Game Over</h2>
      <p>Your Score: {score}</p>
      <button onClick={onRestart}>Restart</button>
    </div>
  );
};

export default GameOverScreen;
