import React from "react";
import "./PauseMenu.css";
const PauseMenu = ({ onResume }) => {
  return (
    <div className="pause-menu">
      <h2>Pause</h2>
      <button onClick={onResume}>Resume</button>
    </div>
  );
};

export default PauseMenu;
