import React, { useState, useEffect, useRef } from "react";
import "./SnakeGame.css"; // Importera CSS-filen för spelet
import StartScreen from "./StartScreen"; // Importera Startsida-komponenten
import GameOverScreen from "./GameOverScreen"; // Importera Game Over-skärmkomponenten
import PauseMenu from "./PauseMenu";
import ExitConfirmationModal from "./ExitConfirmationModal";

const SnakeGame = () => {
  const canvasRef = useRef(null);
  const [snake, setSnake] = useState([
    { x: 10, y: 10 }, // Huvudet
    { x: 6, y: 10 }, // Svans
  ]);
  const [direction, setDirection] = useState("RIGHT");
  const [apples, setApples] = useState([]);
  const [score, setScore] = useState(0);
  const [gameOver, setGameOver] = useState(false);
  const [obstacles, setObstacles] = useState([]);
  const [gameStarted, setGameStarted] = useState(false);
  const [isPaused, setIsPaused] = useState(false);
  const [isPauseMenuVisible, setIsPauseMenuVisible] = useState(false);
  const [lastChangedDirection, setLastChangedDirection] = useState(null);
  const [resumeCounter, setResumeCounter] = useState(null);
  const [showExitModal, setShowExitModal] = useState(false);

  const generateApple = () => {
    const x = Math.floor(Math.random() * (350 / 10)) * 10;
    const y = Math.floor(Math.random() * (410 / 10)) * 10;
    const type = Math.random() < 0.5 ? "red" : "#57c1c5"; // Slumpmässigt välj äpplets typ
    const points = type === "red" ? 1 : 2; // Ange poäng baserat på typ
    return { x, y, type, points };
  };

  const generateObstacle = () => {
    const x = Math.floor(Math.random() * (350 / 10)) * 10;
    const y = Math.floor(Math.random() * (410 / 10)) * 10;
    return { x, y };
  };

  const moveObstacle = (obstacle) => {
    const newObstacle = { ...obstacle };
    newObstacle.x = Math.floor(Math.random() * (350 / 10)) * 10;
    newObstacle.y = Math.floor(Math.random() * (410 / 10)) * 10;
    return newObstacle;
  };

  const startGame = () => {
    setGameStarted(true);
  };

  const handlePauseClick = () => {
    if (gameOver) return;

    setIsPaused((prevIsPaused) => !prevIsPaused);
    setIsPauseMenuVisible((prevIsVisible) => !prevIsVisible);
    setResumeCounter(null); // Återställ räknaren när pausen hävs
  };
  const handleExitClick = () => {
    if (gameOver || !gameStarted) {
      // Avsluta direkt om spelet redan är över eller inte har startat
      setGameStarted(false);
      setApples([]);

      setObstacles([]);
      setSnake([]);
    } else {
      // Visa exit-modal om spelet pågår
      setShowExitModal(true);
    }
  };

  const handleExitCancel = () => {
    setShowExitModal(false);
  };

  const handleExitConfirm = () => {
    setShowExitModal(false);
    setGameStarted(false);
    setApples([]);

    setApples([generateApple(), generateApple()]);
    setSnake([
      { x: 10, y: 10 },
      { x: 6, y: 10 },
    ]);
  };
  const restartGame = () => {
    setSnake([
      { x: 10, y: 10 },
      { x: 6, y: 10 },
    ]);
    setDirection("RIGHT");
    setApples([generateApple(), generateApple()]);
    setObstacles([]);
    setScore(0);
    setGameOver(false);
  };

  const isValidObstaclePosition = (newObstacle) => {
    return !snake.some(
      (segment) => segment.x === newObstacle.x && segment.y === newObstacle.y
    );
  };

  useEffect(() => {
    // Skapa två äpplen när komponenten renderas
    setApples([generateApple(), generateApple()]);

    // Lägg till hinder var 10:e sekund
    const obstacleInterval = setInterval(() => {
      setObstacles((prevObstacles) => {
        const newObstacle = moveObstacle(prevObstacles[0]);
        return isValidObstaclePosition(newObstacle)
          ? [newObstacle]
          : prevObstacles;
      });
    }, 10000);

    if (gameOver) {
      setApples([]);
      setObstacles([]);
    }

    return () => {
      clearInterval(obstacleInterval);
    };
  }, [gameOver]);

  const handleArrowClick = (newDirection) => {
    // Kontrollera om det har gått tillräckligt med tid sedan senaste ändring av riktning
    const now = Date.now();
    if (now - lastChangedDirection < 300) {
      return;
    }

    // Om riktningen inte har ändrats eller om den har ändrats till ett giltigt nytt värde
    if (newDirection !== direction) {
      if (
        (newDirection === "UP" && direction === "DOWN") ||
        (newDirection === "DOWN" && direction === "UP") ||
        (newDirection === "LEFT" && direction === "RIGHT") ||
        (newDirection === "RIGHT" && direction === "LEFT")
      ) {
        return; // Ignorera om motsatt riktning klickas
      }

      setDirection(newDirection);
      setLastChangedDirection(now);

      // Inaktivera motsatta knappar i 300 ms
      const oppositeButtons = {
        UP: "down-button",
        DOWN: "up-button",
        LEFT: "right-button",
        RIGHT: "left-button",
      };

      const oppositeButton = oppositeButtons[newDirection];
      const oppositeButtonElement = document.getElementById(oppositeButton);
      if (oppositeButtonElement) {
        oppositeButtonElement.disabled = true;

        setTimeout(() => {
          if (oppositeButtonElement) {
            oppositeButtonElement.disabled = false;
          }
        }, 300);
      }
    }
  };

  useEffect(() => {
    if (!gameStarted) return;

    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");

    const drawSnake = () => {
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      // Rita hinder
      ctx.fillStyle = "#f6c02a";
      ctx.strokeStyle = "#2d044f"; // Färgen på kanten
      ctx.lineWidth = 2; // Bredden på kanten

      obstacles.forEach((obstacle) => {
        ctx.fillRect(obstacle.x, obstacle.y, 10, 10);
        ctx.strokeRect(obstacle.x, obstacle.y, 10, 10); // Rita kanten runt hinder
      });

      // Rita ormen
      snake.forEach((segment, index) => {
        ctx.fillStyle = index === 0 ? "#ec4360" : "#2d044f";

        // Rita huvudet som en fyrkant
        if (index === 0) {
          ctx.fillRect(segment.x, segment.y, 10, 10);
        } else {
          // Rita svanssegmenten
          ctx.fillStyle = "#2d044f";
          ctx.fillRect(segment.x, segment.y, 10, 10);
        }
      });

      // Rita äpplen
      apples.forEach((apple) => {
        ctx.fillStyle = apple.type; // Färgen för äpplet
        ctx.beginPath();
        ctx.arc(apple.x + 5, apple.y + 5, 5, 0, 2 * Math.PI);
        ctx.fill();
      });

      // Rita återställningsräknaren
      if (resumeCounter !== null) {
        ctx.fillStyle = "#ffffff";
        ctx.font = "20px Arial";
        ctx.fillText(`Resuming in ${resumeCounter}...`, 50, 50);
      }
    };

    const updateSnake = () => {
      if (gameOver) return;

      const newSnake = [...snake];
      const head = { ...newSnake[0] };

      switch (direction) {
        case "UP":
          head.y -= 10;
          break;
        case "DOWN":
          head.y += 10;
          break;
        case "LEFT":
          head.x -= 10;
          break;
        case "RIGHT":
          head.x += 10;
          break;
        default:
          break;
      }

      // Kolla om ormen kolliderar med väggarna
      if (head.y < 0) {
        head.y = 410; // Om ormen går upp genom översta kanten, placera den nederst
      } else if (head.y >= 420) {
        head.y = 0; // Om ormen går ner genom nedersta kanten, placera den högst upp
      }

      if (head.x < 0) {
        head.x = 340; // Om ormen går vänster genom vänstra kanten, placera den längst till höger
      } else if (head.x >= 350) {
        head.x = 0; // Om ormen går höger genom högra kanten, placera den längst till vänster
      }

      // Kolla om ormen kolliderar med hinder
      if (
        obstacles.some(
          (obstacle) => obstacle.x === head.x && obstacle.y === head.y
        )
      ) {
        setGameOver(true);
        return;
      }

      // Kolla om ormen kolliderar med sig själv
      if (
        newSnake
          .slice(1)
          .some((segment) => segment.x === head.x && segment.y === head.y)
      ) {
        setGameOver(true);
        return;
      }

      newSnake.unshift(head);

      // Kolla om ormen äter ett äpple
      apples.forEach((apple, index) => {
        if (head.x === apple.x && head.y === apple.y) {
          // Ta bort äpplet från listan
          setApples((prevApples) => {
            const newApples = [...prevApples];
            newApples.splice(index, 1);
            return newApples;
          });

          // Lägg till poängen på ormen (simulera poängsystem)
          setScore((prevScore) => prevScore + apple.points);

          // Lägg till ett nytt segment på ormen för varje poäng
          for (let i = 0; i < apple.points; i++) {
            newSnake.push({ ...head });
          }

          // Generera ett nytt äpple
          setApples((prevApples) => [...prevApples, generateApple()]);
        }
      });

      // Ormen växer när den äter äpplen
      if (newSnake.length > snake.length) {
        newSnake.pop(); // Ta bort sista segmentet om ormen växer
      }

      setSnake(newSnake);
    };

    if (!gameStarted || isPaused) return;

    const handleKeyPress = (e) => {
      if (gameOver) {
        if (e.key === "Enter") {
          restartGame();
        }
        return;
      }

      switch (e.key) {
        case "ArrowUp":
          if (direction !== "DOWN") {
            setDirection("UP");
          }
          break;
        case "ArrowDown":
          if (direction !== "UP") {
            setDirection("DOWN");
          }
          break;
        case "ArrowLeft":
          if (direction !== "RIGHT") {
            setDirection("LEFT");
          }
          break;
        case "ArrowRight":
          if (direction !== "LEFT") {
            setDirection("RIGHT");
          }
          break;
        default:
          break;
      }
    };

    window.addEventListener("keydown", handleKeyPress);

    const gameLoop = () => {
      updateSnake();
      drawSnake();
    };

    const gameInterval = setInterval(() => {
      if (resumeCounter !== null) {
        setResumeCounter((prevCounter) =>
          prevCounter !== 0 ? prevCounter - 1 : null
        );
      } else {
        gameLoop();
      }
    }, 100);

    return () => {
      clearInterval(gameInterval);
      window.removeEventListener("keydown", handleKeyPress);
    };
  }, [
    snake,
    direction,
    apples,
    score,
    gameOver,
    obstacles,
    gameStarted,
    isPaused,
    resumeCounter,
  ]);

  return (
    <div className="game-wrapper">
      <div className="snake-game-container">
        {gameStarted ? (
          <>
            <div className="scoreText">Score: {score}</div>
            <button className="exit-button" onClick={handleExitClick}>
              X
            </button>
            <div className="canvas-container">
              <canvas
                ref={canvasRef}
                width={350}
                height={420}
                className="game-canvas"
              />
            </div>
            <div className="joystick">
              <div
                className="arrow-button up-button"
                onMouseDown={() => handleArrowClick("UP")}
              >
                &#8593;
              </div>
              <div className="arrow-row">
                <div
                  className="arrow-button left-button"
                  onMouseDown={() => handleArrowClick("LEFT")}
                >
                  &#8592;
                </div>
                <div
                  className={`center-button ${isPaused ? "paused" : "playing"}`}
                  onMouseDown={() => handlePauseClick()}
                >
                  {isPaused ? "R" : "P"}
                </div>
                <div
                  className="arrow-button right-button"
                  onMouseDown={() => handleArrowClick("RIGHT")}
                >
                  &#8594;
                </div>
              </div>
              <div
                className="arrow-button down-button"
                onMouseDown={() => handleArrowClick("DOWN")}
              >
                &#8595;
              </div>
            </div>
            {resumeCounter !== null && (
              <div className="resume-counter">
                Resuming in {resumeCounter}...
              </div>
            )}
          </>
        ) : (
          <StartScreen onStart={startGame} />
        )}
        {isPauseMenuVisible && (
          <PauseMenu
            onResume={() => handlePauseClick()}
            onRestart={restartGame}
          />
        )}
        {gameOver && <GameOverScreen score={score} onRestart={restartGame} />}
        {showExitModal && (
          <ExitConfirmationModal
            onCancel={handleExitCancel}
            onConfirm={handleExitConfirm}
          />
        )}
      </div>
    </div>
  );
};

export default SnakeGame;
