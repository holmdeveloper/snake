import React from "react";
import "./StartScreen.css";
import logo from "./assets/logosnakey.png";

const StartScreen = ({ onStart }) => {
  return (
    <div className="start-screen">
      <div className="start-content">
        <img src={logo} alt="logo" width={250} />
        <button onClick={onStart}>Start game</button>
      </div>
    </div>
  );
};

export default StartScreen;
